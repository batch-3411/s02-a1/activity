
while True:
    try:
        year = int(input("Please input a year: "))
        if year <= 0:
            raise ValueError
        break
    except ValueError:
        print("Please enter a valid positive integer year.")

if year % 4 == 0:
    if year % 100 == 0:
        if year % 400 == 0:
            print(f"{year} is a leap year.")
        else:
            print(f"{year} is not a leap year.")
    else:
        print(f"{year} is a leap year.")
else:
    print(f"{year} is not a leap year.")

row = int(input("Enter number of rows: "))
col = int(input("Enter number of columns: "))

if row <= 0 or col <= 0:
    print("Rows and columns should be positive integers.")
else:
    for i in range(row):
        for j in range(col):
            print("*", end=" ")
        print()
